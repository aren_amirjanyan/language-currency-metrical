<?php
/*
Plugin Name: LCM (Language Currency Metrical)
Plugin URI: http://currency.dev
Description: Language, Currency and Metrical Converter.
Version: 1.0
Author: Aren Amirjanyan
Author URI: http://
*/

require_once(plugin_dir_path(__FILE__) . 'widgets/language-currency-metrical-switcher.php');

class LCM
{

    static private $base_language = 'en';
    static private $base_currency = 'ILS';
    static private $base_measurement = 'M';
    static private $base_square_measurement = 'SqM';
    private $currency_api_endpoint;
    private $currency_api_url;
    private $metric;

    /**
     * Constructor of LCM Plugin
     */
    public function __construct()
    {

        $this->currency_api_url = esc_attr(get_option('currency_api_url'));
        $this->metric = require_once(plugin_dir_path(__FILE__) . 'config/metric.php');
        session_start();
        $_SESSION['language'] = (!empty($_SESSION['language'])) ? $_SESSION['language'] : self::$base_language;
        $_SESSION['currency'] = (!empty($_SESSION['currency'])) ? $_SESSION['currency'] : self::$base_currency;
        $_SESSION['measurement'] = (!empty($_SESSION['measurement'])) ? $_SESSION['measurement'] : self::$base_measurement;
        $_SESSION['square_string'] = (!empty($_SESSION['square_string'])) ? $_SESSION['square_string'] : self::$base_square_measurement;

        add_action('admin_menu', array($this, 'currencySettingsMenu'));
        add_action('widgets_init', array($this, 'loadLanguageAndCurrencySwitcherWidget'));
        // Register style sheet.
        add_action('wp_enqueue_scripts', array($this, 'currencyPluginStyles'));
        add_action('wp_enqueue_scripts', array($this, 'currencyPluginScripts'));

        register_sidebar(array(
            'name' => __('Language, Currency and Metrical', 'theme'),
            'id' => 'language-currency-metrical-widget-area',
            'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
            'after_widget' => '</li>',
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        ));
        add_action('wp_ajax_change_language_and_currency', array($this, 'changeLanguageAndCurrency'));
        add_action('wp_ajax_nopriv_change_language_and_currency', array($this, 'changeLanguageAndCurrency'));
        add_action('wp_ajax_change_currency_values', array($this, 'changeCurrencyValues'));
        add_action('wp_ajax_nopriv_change_currency_values', array($this, 'changeCurrencyValues'));
        add_shortcode('convert-price', array($this, 'convertPrice'));
        add_shortcode('convert-metrical', array($this, 'convertMetrical'));
        add_shortcode('convert-square-metrical', array($this, 'convertSquareMetrical'));
        add_shortcode('show-lcm-loader', array($this, 'lcmLoader'));


    }
    function lcmLoader($args){
        $lcm_loader_html = '<div id="'.$args['id'].'" style="background-image: url('.plugins_url('/images/loading.gif', __FILE__).')" ></div>';
        echo $lcm_loader_html;
    }

    /**
     * Converted the price value
     * @param $args
     * @return float
     */
    function convertPrice($args)
    {
        $coefficient = !empty($_SESSION['currency_coefficient']) ? $_SESSION['currency_coefficient'] : 1.00;
		$currency = $_SESSION['currency'];
        $newPriceValue = $coefficient * $args['value'];
		$baseCurrencyClass = esc_attr(get_option('currency_class'));
		$html = "<span class='short-code-$baseCurrencyClass'>$newPriceValue</span><span> $currency </span>";
		echo $html;
    }
	 /**
     * Converted the metrical value
     * @param $args
     * @return float
     */
	function convertMetrical($args){
		$coefficient = !empty($_SESSION['metric_coefficient']) ? $_SESSION['metric_coefficient'] : 1.00;
		$mtrical = $_SESSION['measurement'];
		$newMetricalValue = number_format($args['value'] * $coefficient,2,',',' ');
		$baseMetricalClass = esc_attr(get_option('measurement_class'));
		$html = "<span class='short-code-$baseMetricalClass'>$newMetricalValue</span><span> $mtrical </span>";
		echo $html;
	}
	 /**
     * Converted the square metrical value
     * @param $args
     * @return float
     */
	function convertSquareMetrical($args){
		
		$coefficient = !empty($_SESSION['square_metric_coefficient']) ? $_SESSION['square_metric_coefficient'] : 1.00;
		$square_string = $_SESSION['square_string'];
		$newMetricalSquareValue = ($coefficient === 1 )? number_format($coefficient*$args['value'],2,',',' ') : number_format( $coefficient*sqrt($args['value']),2,',',' ');
		$baseMetricalSquareClass = esc_attr(get_option('measurement_square_class'));
		$html = "<span class='short-code-$baseMetricalSquareClass'>$newMetricalSquareValue</span><span> $square_string </span>";
		echo $html;
	}

    /**
     * change language and currency
     */
    function changeLanguageAndCurrency()
    {

        $data = array();
        $success = false;
        $error = true;
        $message = 'Something went wrong';

        if (!empty($_POST['data'])) {
            $_SESSION['language'] = (!empty($_POST['data']['language']) && $_POST['data']['language'] != 'default') ? $_POST['data']['language'] : self::$base_language;
            $_SESSION['currency'] = (!empty($_POST['data']['currency']) && $_POST['data']['currency'] != 'default') ? $_POST['data']['currency'] : self::$base_currency;
            $_SESSION['measurement'] = (!empty($_POST['data']['measurement']) && $_POST['data']['measurement'] != 'default') ? $_POST['data']['measurement'] : self::$base_measurement;

            $this->setCoefficient($this->getCurrencyCoefficient(), $this->getMetricCoefficient(),$this->getSquareMetricCoefficient());

            $success = true;
            $error = false;
            $message = 'The Language and Currency successfully updated';
            $data = array(
                'language' => $_SESSION['language'],
                'currency' => $_SESSION['currency'],
                'measurement' => $_SESSION['measurement'],
                'currency_coefficient' => $_SESSION['currency_coefficient'],
                'metric_coefficient' => $_SESSION['metric_coefficient'],
                'square_metric_coefficient' => $_SESSION['square_metric_coefficient'],
                'square_string' => $_SESSION['square_string'],
            );
            //do_action('wpml_switch_language', $_SESSION['language']); not working for this version
        }
        echo $this->api_response($data, $success, $error, $message);
        wp_die();
    }

    /**
     * Set the values of coefficients in SESSION
     * @param float $currency_coefficient
     * @param float $metric_coefficient
     * @param float $square_metric_coefficient
     */
    function setCoefficient($currency_coefficient = 1.00, $metric_coefficient = 1.00,$square_metric_coefficient = 1.00)
    {
        $_SESSION['currency_coefficient'] = $currency_coefficient;
        $_SESSION['metric_coefficient'] = $metric_coefficient;
        $_SESSION['square_metric_coefficient'] = $square_metric_coefficient;
    }

    /**
     * Returned coefficient of square metric values
     * @return float
     */
    function getSquareMetricCoefficient()
    {
        $base_metric =  esc_attr(get_option('base_metric'));
        $_SESSION['square_string'] = $this->metric[$_SESSION['measurement']]['square'][$_SESSION['language']];
        return sqrt(pow($this->metric[$base_metric]['coefficient'] / $this->metric[$_SESSION['measurement']]['coefficient'],2));
    }

    /**
     * Returned coefficient of metric values
     * @return float
     */
    function getMetricCoefficient()
    {
        $base_metric =  esc_attr(get_option('base_metric'));
        return $this->metric[$base_metric]['coefficient'] / $this->metric[$_SESSION['measurement']]['coefficient'];
    }

    /**
     * Returned coefficient of currency
     * @return float
     */
    function getCurrencyCoefficient()
    {
        $currency = esc_attr(get_option('base_currency'));
        $this->currency_api_endpoint = 'latest';
        $response = json_decode($this->getCoefficientsOfCurrency($currency), true);
        if (!empty($response['rates'])) {
            $coefficient = $response['rates'][$_SESSION['currency']];
        }

        $coefficient = !empty($coefficient) ? $coefficient : 1.00;

        return $coefficient;

    }

    /**
     * Changed the currency values
     */
    function changeCurrencyValues()
    {
        $data = array();
        $success = false;
        $error = true;
        $message = 'Something went wrong';

        if (!empty($_POST['data']) && $_POST['data']['convert'] == 'true') {

            $success = true;
            $error = false;
            $message = 'The Coefficient of Currency successfully calculated';

            $data = array(
                'currency_coefficient' => !empty($_SESSION['currency_coefficient']) ? $_SESSION['currency_coefficient'] : 1.00,
                'currency' => $_SESSION['currency']
            );
        }
        echo $this->api_response($data, $success, $error, $message);
        wp_die();
    }

    /**
     * Registered the plugin styles
     */
    function currencyPluginStyles()
    {
        wp_register_style('currency-plugin-style', plugins_url('/css/style.css', __FILE__));
        wp_enqueue_style('currency-plugin-style');
    }

    /**
     * Registered the plugin scripts
     */
    function currencyPluginScripts()
    {
        wp_register_script('currency-plugin-jquery', plugins_url('/js/jquery-3.2.1.min.js', __FILE__));
        wp_enqueue_script('currency-plugin-jquery');
        wp_register_script('currency-plugin-script', plugins_url('/js/scripts.js', __FILE__));
        wp_enqueue_script('currency-plugin-script');
        wp_localize_script('currency-plugin-script', 'WP_AJAX',
            array(
                'ajax_url' => admin_url('admin-ajax.php'),
                'currency_class' => esc_attr(get_option('currency_class')),
                'measurement_class' => esc_attr(get_option('measurement_class')),
                'measurement_square_class' => esc_attr(get_option('measurement_square_class'))
            )
        );
    }

    /**
     * Loaded the language and currency switcher widget
     */
    function loadLanguageAndCurrencySwitcherWidget()
    {
        register_widget('LCMSwitcher');
    }

    /**
     * Currency Settings Menu in admin dashboard
     */
    function currencySettingsMenu()
    {
        //create new top-level menu
        add_menu_page('Currency Settings', 'Currency Settings', 'administrator', __FILE__, array($this, 'currencySettingsPage'), plugins_url('/images/icon.png', __FILE__));
        //call register settings function
        add_action('admin_init', array($this, 'registerCurrencyPluginSettings'));
    }

    /**
     * Registered the currency plugin settings
     */
    function registerCurrencyPluginSettings()
    {
        register_setting('currency-plugin-settings-group', 'currency_api_url');
        register_setting('currency-plugin-settings-group', 'base_currency');
        register_setting('currency-plugin-settings-group', 'base_metric');
        register_setting('currency-plugin-settings-group', 'currency_class');
        register_setting('currency-plugin-settings-group', 'measurement_class');
        register_setting('currency-plugin-settings-group', 'measurement_square_class');
    }

    /**
     * Returned HTML for currency settings page
     */
    function currencySettingsPage()
    {
        ?>
        <div class="wrap">
            <h1>Currency</h1>

            <form method="post" action="options.php">
                <?php settings_fields('currency-plugin-settings-group'); ?>
                <?php do_settings_sections('currency-plugin-settings-group'); ?>
                <table class="form-table">
                    <tr valign="top">
                        <th scope="row">Currency API URL</th>
                        <td><input type="text" name="currency_api_url"
                                   value="<?php echo esc_attr(get_option('currency_api_url')); ?>"/></td>
                    </tr>

                    <tr valign="top">
                        <th scope="row">Base Currency</th>
                        <td><input type="text" name="base_currency"
                                   value="<?php echo esc_attr(get_option('base_currency')); ?>"/></td>
                    </tr>
                    <tr valign="top">
                        <th scope="row">Base Metric</th>
                        <td><input type="text" name="base_metric"
                                   value="<?php echo esc_attr(get_option('base_metric')); ?>"/></td>
                    </tr>

                    <tr valign="top">
                        <th scope="row">Currency Class</th>
                        <td><input type="text" name="currency_class"
                                   value="<?php echo esc_attr(get_option('currency_class')); ?>"/></td>
                    </tr>
                    <tr valign="top">
                        <th scope="row">Measurement Class</th>
                        <td><input type="text" name="measurement_class"
                                   value="<?php echo esc_attr(get_option('measurement_class')); ?>"/></td>
                    </tr>
                    <tr valign="top">
                        <th scope="row">Measurement Square Class</th>
                        <td><input type="text" name="measurement_square_class"
                                   value="<?php echo esc_attr(get_option('measurement_square_class')); ?>"/></td>
                    </tr>
                </table>

                <?php submit_button(); ?>

            </form>
        </div>
        <?php
    }

    /**
     * Returned coefficients of currency via API
     * @param string $currency
     * @return mixed
     */
    function getCoefficientsOfCurrency($currency)
    {

        $url = $this->currency_api_url . $this->currency_api_endpoint;

        $response = $this->curl($url, array('base' => $currency));

        return $response;

    }

    /**
     * Returned API response via curl
     * @param string $url
     * @param array $parameters
     * @return mixed
     */
    function curl($url, $parameters)
    {

        $query = http_build_query($parameters);

        $full_url = $url . '?' . $query;

        // create curl resource
        $ch = curl_init();

        // set url
        curl_setopt($ch, CURLOPT_URL, $full_url);

        //return the transfer as a string
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        // $output contains the output string
        $output = curl_exec($ch);

        // close curl resource to free up system resources
        curl_close($ch);

        return $output;
    }

    /**
     * Returned json response
     * @param array $data
     * @param bool|true $success
     * @param bool|false $error
     * @param string $message
     * @return mixed|string|void
     */
    function api_response($data = [], $success = true, $error = false, $message = '')
    {
        return json_encode(array(
            'data' => $data,
            'success' => $success,
            'error' => $error,
            'message' => $message
        ));
    }
}
new LCM();
?>