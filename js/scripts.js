/**
 * Created by Admin on 09.05.2017.
 */
jQuery(document).ready(function () {
    var lcmLoader = $('#lcm-loader');
    var redirect_url = '';
    var ajaxurl = WP_AJAX.ajax_url;
    var CurrencyClass = WP_AJAX.currency_class;
    var measurementClass = WP_AJAX.measurement_class;
    var measurementSquareClass = WP_AJAX.measurement_square_class;

    // changeCurrencyValuesFromSession();
    changeCurrencyValuesFromLocalStorage();

    jQuery(document).on('click', '#change-language-and-currency', function () {
        lcmLoader.show();
        var language = jQuery('#language').val();
        var currency = jQuery('#currency').val();
        var measurement = jQuery('#measurement').val();
        var redirect_url = jQuery('#language').attr('data-url');
        jQuery.post(
            ajaxurl,
            {
                action: 'change_language_and_currency',
                data: {
                    language: language,
                    currency: currency,
                    measurement: measurement
                }
            },
            function (data) {
                var response = jQuery.parseJSON(data);
                if (response.data && response.success) {
                    setSessionDataToLocalStorage('lac_session_data', response.data);
                    window.location.href = redirect_url;
                }
            }
        );
    });
    jQuery(document).on('change', '#language', function () {
        var optionSelected = jQuery("option:selected", this);
        var data_url = optionSelected.attr('data-url');
        jQuery(this).attr('data-url', data_url);
    });

    function changeCurrencyValuesFromSession() {
        jQuery.post(
            ajaxurl,
            {
                action: 'change_currency_values',
                data: {
                    convert: true
                }
            },
            function (data) {
                var response = jQuery.parseJSON(data);
                if (response.data && response.success) {
                    var currency_coefficient = parseFloat(response.data.currency_coefficient);
                    var currency_values = jQuery('.' + CurrencyClass);
                    jQuery.each(currency_values, function (key, value) {
                        var oldCurrencyValue = parseFloat(jQuery(value).text());
                        var newCurrencyValue = currency_coefficient * oldCurrencyValue;
                        jQuery(value).text(newCurrencyValue.toFixed(2));
                        jQuery(value).append('<span> ' + response.data.currency + ' </span>')
                    });
                }
            }
        );
    }

    function setSessionDataToLocalStorage(storageKey, sessionData) {
        //lac_session_data - language and currency session data
        localStorage.setItem(storageKey, JSON.stringify(sessionData));
    }

    function getSessionDataFromLocalStorage(storageKey) {
        var lac_session_data = $.parseJSON(localStorage.getItem(storageKey));
        if (!lac_session_data) {
            lac_session_data = {
                language: jQuery('#language').val(),
                currency: jQuery('#currency').val(),
                measurement: jQuery('#measurement').val(),
                currency_coefficient: 1,
                metric_coefficient: 1,
                square_metric_coefficient:1,
                square_string:'SqM'
            }
        }
        return lac_session_data;
    }

    function changeCurrencyValuesFromLocalStorage() {

        var lac_session_data = getSessionDataFromLocalStorage('lac_session_data');

        var currency_coefficient = parseFloat(lac_session_data.currency_coefficient);
        var metric_coefficient = parseFloat(lac_session_data.metric_coefficient);
        var square_metric_coefficient = parseFloat(lac_session_data.square_metric_coefficient);

        var currency_values = jQuery('.' + CurrencyClass);

        var measurement_values = jQuery('.' + measurementClass);

        var measurement_square_values = jQuery('.' + measurementSquareClass);

        jQuery.each(currency_values, function (key, value) {
            var oldCurrencyValue = parseFloat(jQuery(value).text());
            var newCurrencyValue = currency_coefficient * oldCurrencyValue;
            jQuery(value).text(newCurrencyValue.toFixed(2));
            jQuery(value).append('<span> ' + lac_session_data.currency + ' </span>')
        });
        jQuery.each(measurement_values, function (key, value) {
            var oldMeasurementValue = parseFloat(jQuery(value).text());
            var newMeasurementValue = metric_coefficient * oldMeasurementValue;
            jQuery(value).text(newMeasurementValue.toFixed(2));
            jQuery(value).append('<span> ' + lac_session_data.measurement + ' </span>');
        });
        jQuery.each(measurement_square_values, function (key, value) {
            var oldMeasurementSquareValue = parseFloat(jQuery(value).text());
            var newMeasurementSquareValue = square_metric_coefficient * Math.sqrt(oldMeasurementSquareValue);
            if(square_metric_coefficient === 1){
                newMeasurementSquareValue = square_metric_coefficient * oldMeasurementSquareValue;
            }
            jQuery(value).text(newMeasurementSquareValue.toFixed(2));
            jQuery(value).append('<span class="square_string"> ' + lac_session_data.square_string + ' </span>');
        });
    }

});