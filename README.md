# README #

LCM (Language, Currency and Metrical) Wordpress Plugin.

### How install the plugin? ###

* Download plugin
* Copy in wordpress plugins folder (/wp-content/plugins/)
* Activate the plugin

### How make the settings? ###

* Go Currency settings page(in admin dashboard)
* Enter the options values 
* For Example : 
Currency API URL- http://api.fixer.io/
Base Currency - ILS
Base Metric - M
Currency Class - price-currency
Measurement Class - distance
Measurement Square Class - square-distance
* Go widgets section
* Select the LCM (Language, Currency and Metrical) Switcher and move  it in Language, Currency and Metrical sidebar section
* Enter the options

### How see the result? ###

* Run [show-lcm-loader id="lcm-loader"] short code for page loader - OPTIONAL 
* Copy dynamic_sidebar('language-currency-metrical-widget-area') in HTML - Required