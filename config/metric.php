<?php
return array(
    "M" => [
        "name" => "Meter",
        "square" => [
            "en" => "SqM",
            "he" => "SqM"
        ],
        "coefficient" => 1
    ],
    "KM" => [
        "name" => "Kilometer",
        "square" => [
            "en" => "SqKm",
            "he" => "SqKm"
        ],
        "coefficient" => 1000
    ],
    "CM" => [
        "name" => "Centimeter",
        "square" => [
            "en" => "SqCm",
            "he" => "SqCm"
        ],
        "coefficient" => 0.01
    ],
    "MilM" => [
        "name" => "Millimeter",
        "square" => [
            "en" => "SqMilM",
            "he" => "SqMilM"
        ],
        "coefficient" => 0.001
    ],
    "MicM" => [
        "name" => "Micrometer",
        "square" => [
            "en" => "SqMicM",
            "he" => "SqMicM"
        ],
        "coefficient" => 0.000001
    ],
    "NM" => [
        "name" => "Nanometer",
        "square" => [
            "en" => "SqNm",
            "he" => "SqNm"
        ],
        "coefficient" => 0.000000001
    ],
    "Mile" => [
        "name" => "Mile",
        "square" => [
            "en" => "SqMile",
            "he" => "SqMile"
        ],
        "coefficient" => 1609.35
    ],
    "Y" => [
        "name" => "Yard",
        "square" => [
            "en" => "SqYard",
            "he" => "SqYard"
        ],
        "coefficient" => 0.9144
    ],
    "F" => [
        "name" => "Foot",
        "square" => [
            "en" => "SqFoot",
            "he" => "SqFoot"
        ],
        "coefficient" => 0.3048
    ],
    "I" => [
        "name" => "Inch",
        "square" => [
            "en" => "SqI",
            "he" => "SqI"
        ],
        "coefficient" => 0.0254
    ],
    "LY" => [
        "name" => "Light Year",
        "square" => [
            "en" => "SqLY",
            "he" => "SqLY"
        ],
        "coefficient" => 9.46066e+15
    ],
);
