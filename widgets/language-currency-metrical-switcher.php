<?php

/**
 * Class LCMSwitcher
 */
class LCMSwitcher extends WP_Widget
{
    function __construct()
    {
        parent::__construct(
            'lcm_switcher',
            __('LCM (Language, Currency and Metrical) Switcher', 'language_currency_metrical_switcher_domain'),
            array('description' => __('LCM (Language, Currency and Metrical) switcher for changing of default language, currency and metrical values', 'language_currency_metrical_switcher_domain'),)
        );
    }

    /**
     * Creating widget front-end
     * This is where the action happens
     * @param array $args
     * @param array $instance
     */
    public function widget($args, $instance)
    {
        session_start();
        $languagesOptions = unserialize($instance['languages']);
        $currencyOptions = explode(',', str_replace(' ', '', $instance['currencies']));
        $measurementOptions = explode(',', str_replace(' ', '', $instance['measurement']));

        $currencyOptionsHTML = '';
        $measurementOptionsHTML = '';

        if (!empty($currencyOptions)) {
            foreach ($currencyOptions as $currencyOption) {
                $selected = '';
                if ($currencyOption == $_SESSION['currency'])
                    $selected = 'selected="selected"';
                $currencyOptionsHTML .= '<option class="currency-option" value="' . $currencyOption . '" ' . $selected . '>' . $currencyOption . '</option>';
            }
        }
        if (!empty($measurementOptions)) {
            foreach ($measurementOptions as $measurementOption) {
                $selected = '';
                if ($measurementOption == $_SESSION['measurement'])
                    $selected = 'selected="selected"';
                $measurementOptionsHTML .= '<option class="measurement-option" value="' . $measurementOption . '" ' . $selected . '>' . $measurementOption . '</option>';
            }
        }


        $active_language_url = '';
        $current_language_name = 'English';
        $languages_html = '';

        if (!empty($languagesOptions)) {
            foreach ($languagesOptions as $language) {
                $selected = '';
                if ($language['code'] == $_SESSION['language']) {
                    $current_language_name = $language['native_name'];
                    $selected = 'selected="selected"';
                    $active_language_url = $language['url'];
                }
                $languages_html .= '<option class="language-option" value="' . $language['code'] . '" ' . $selected . ' data-img="' . $language['country_flag_url'] . '" data-url="' . $language['url'] . '">' . $language['native_name'] . '</option>';
            }
        }
        $language_currency_metrical_switcher = '<form action="" method="post"><div class="lcm-switcher">
					<label> ' . $current_language_name . ' (' . $_SESSION['currency'] . '/INC)</label>
					<div>
					    <select name="language" id="language" data-url="' . $active_language_url . '">
					        <option value="default">Languages</option>
						    ' . $languages_html . '
						</select>
						<select name="currency" id="currency">
							<option value="default">Currencies</option>
							' . $currencyOptionsHTML . '
						</select>
						<select name="measurement" id="measurement">
							<option value="default">Measurement</option>
							' . $measurementOptionsHTML . '
						</select>
						<button type="button" id="change-language-and-currency">Update</button>
					</div>
				</div></form>';
        echo __($language_currency_metrical_switcher, 'language_currency_metrical_switcher_domain');
    }
    /**
     * Widget Backend
     * @param array $instance
     */
    public function form($instance)
    {
        if (isset($instance['languages'])) {
            $languages = unserialize($instance['languages']);
            $codes = [];
            if(!empty($languages)){
                foreach($languages as $key=> $value){
                    $codes[] = $value['code'];
                }
            }
        } else {
            $languages = [];
            $codes = [];
        }
        if (isset($instance['currencies'])) {
            $currencies = $instance['currencies'];
        } else {
            $currencies = __('Currencies', 'language_currency_metrical_switcher_domain');
        }
        if (isset($instance['measurement'])) {
            $measurement = $instance['measurement'];
        } else {
            $measurement = __('Measurement', 'language_currency_metrical_switcher_domain');
        }

        $wpml_languages = apply_filters('wpml_active_languages', NULL, 'orderby=id&order=asc');

        $languages_html = '';

        if (!empty($wpml_languages)) {
            foreach ($wpml_languages as $language) {
                $checked = '';
                if (in_array($language['code'], $codes)) {
                    $checked = 'checked = "checked"';
                }
                $languages_html .= '<div id="' . $this->get_field_id('languages') . '-' . $language['code'] . '" style="margin-top: 5px;"><input id="'.$this->get_field_id('languages').'-checkbox-' . $language['code'] . '" type="checkbox" class="widefat" name="' . $this->get_field_name('languages') . '[]" value="' . $language['code'] . '" ' . $checked . '/><label for="'.$this->get_field_id('languages').'-checkbox-' . $language['code'] . '">' . $language['native_name'] . '</label></div>';
            }
        }
        ?>
        <p>
            <label for="<?php echo $this->get_field_id('languages'); ?>"><?php _e('Languages:'); ?></label>
            <?php echo $languages_html; ?>
        </p>
        <p style="color:black; margin: 1em 0 0 0;font-size: 10px">
            Please select the languages.
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('currencies'); ?>"><?php _e('Currencies:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('currencies'); ?>"
                   name="<?php echo $this->get_field_name('currencies'); ?>" type="text"
                   value="<?php echo esc_attr($currencies); ?>"/>
        </p>
        <p style="color:black; margin: 1em 0 0 0;font-size: 10px">
            Please use comma to separate values.
        </p>

        <p>
            <label for="<?php echo $this->get_field_id('measurement'); ?>"><?php _e('Measurement:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('measurement'); ?>"
                   name="<?php echo $this->get_field_name('measurement'); ?>" type="text"
                   value="<?php echo esc_attr($measurement); ?>"/>
        </p>
        <p style="color:black; margin: 1em 0 0 0;font-size: 10px">
            Please use comma to separate values (for example : <i> M,KM,CM,MilM,MicM,NM,Mile,Y,F,I,LY </i>).
        </p>

        <?php
    }

    /**
     * @param array $new_instance
     * @param array $old_instance
     * @return array
     */
    public function update($new_instance, $old_instance)
    {
        $instance = array();

        $serializeLanguages = [];

        $wpml_languages = apply_filters('wpml_active_languages', NULL, 'orderby=id&order=asc');

        if (!empty($wpml_languages)) {
            foreach ($wpml_languages as $language) {
                if (in_array($language['code'], $new_instance['languages'])) {
                    $serializeLanguages[] = $language;
                }
            }
        }
        $instance['languages'] = serialize($serializeLanguages);
        $instance['currencies'] = (!empty($new_instance['currencies'])) ? strip_tags($new_instance['currencies']) : '';
        $instance['measurement'] = (!empty($new_instance['measurement'])) ? strip_tags($new_instance['measurement']) : '';
        return $instance;
    }
}
